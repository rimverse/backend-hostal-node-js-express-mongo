const { Habitacion, Usuario, Rol } = require("../models");


const coleccionesPermitidas = (coleccion = '', colecciones = []) => {

    if (!colecciones.includes(coleccion)) {
        throw new Error(`La colección ${coleccion.toUpperCase()} no esta permitida: Solo -> ${colecciones}`);
    };
    return true;
};

const esRolValido = async (rol = '') => {
    const existeRol = await Rol.findOne({ rol });
    if (!existeRol) {
        throw new Error(`El rol: ${rol.toUpperCase()} no existe en la base de datos`)
    };
    return true;
};

const existeDni = async (dni = '') => {
    const dniExiste = await Usuario.findOne({ dni });
    if (dniExiste) {
        throw new Error(`El dni ${dni} ya está registrado`);
    };
    return true;
};

const existeEmail = async (correo = '') => {
    const email = await Usuario.findOne({ correo });
    if (email) {
        throw new Error(`El correo: ${correo} ya está registrado`);
    };
    return true;
};

const existeHabitacionPorId = async (id = '') => {
    const existeId = await Habitacion.findById(id);
    if (!existeId) {
        throw new Error(`El id: ${id} no existe en la base de datos`);
    };
    return true;
};

const existeUsuarioPorId = async (id = '') => {
    const existeUsuario = await Usuario.findById(id);
    if (!existeUsuario) {
        throw new Error(`El id ${id} no existe en la base de datos`);
    };
    return true;
};

const obtenerModeloImagen = async (coleccion = '', id) => {

    let modelo;
    switch (coleccion) {
        case 'usuarios':
            modelo = await Usuario.findById(id);
            break;

        case 'habitaciones':
            modelo = await Habitacion.findById(id);
            break;
    };

    return (modelo) ? modelo : null;
};

module.exports = {
    coleccionesPermitidas,
    esRolValido,
    existeDni,
    existeEmail,
    existeHabitacionPorId,
    existeUsuarioPorId,
    obtenerModeloImagen
}