const imgs = ['jpg', 'jpeg', 'png'];

const catalogarArchivo = (nombreArchivo = '') => {

    const nombreCortado = nombreArchivo.split('.');
    const extension = nombreCortado[nombreCortado.length - 1];
    return (imgs.includes(extension)) ? { tipo: 'img', extension } : { tipo: 'doc', extension };
};

module.exports = {
    catalogarArchivo
}