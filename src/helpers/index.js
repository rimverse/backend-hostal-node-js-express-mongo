const subirArchivo = require('./subir-archivo');
const dbValidators = require('./dbValidators');
const catalogarArchivo = require('./catalogar-archivo');
const generarJWT = require('./generar-jwt');

module.exports = {
    ...catalogarArchivo,
    ...dbValidators,
    ...generarJWT,
    ...subirArchivo,
};