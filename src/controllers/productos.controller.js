const { request, response } = require('express');
const { Producto } = require('../models');

const listarProductos = async (req = request, res = response) => {

    const query = { disponible: true };

    const [total, productos] = await Promise.all([
        Producto.countDocuments(query),
        Producto.find(query).populate('categoria', 'nombre')
    ]);

    res.json({
        total, productos
    });
};

const crearProducto = async (req = request, res = response) => {

    const { nombre, categoria, precio, cantidad } = req.body;
    const producto = new Producto({ nombre: nombre.substring(0, 1).toUpperCase() + nombre.substring(1), categoria, precio, cantidad });
    await producto.save();
    res.status(201).json(producto);

};

module.exports = {
    crearProducto,
    listarProductos
}



