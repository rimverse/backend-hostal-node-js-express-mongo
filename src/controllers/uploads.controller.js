const fs = require('fs');
const path = require('path');
const { request, response } = require('express');

const { subirArchivo, obtenerModeloImagen, catalogarArchivo } = require('../helpers');

const cargarArchivos = async (req = request, res = response) => {

    const { coleccion } = req.params;

    try {
        const nombreArchivo = await subirArchivo(req.files, coleccion);
        res.status(201).json({
            nombreArchivo
        });

    } catch (msg) {
        res.status(400).json({ msg });
    };
};

const actualizarImagen = async (req = request, res = response) => {

    const { coleccion, id } = req.params;
    const modelo = await obtenerModeloImagen(coleccion, id);

    if (!modelo) {
        return res.status(404).json({ msg: `El id: ${id} de la colección: ${coleccion.toUpperCase()} no existe` });
    };

    //Limpiar imagenes previas
    if (modelo.img) {

        const { tipo } = catalogarArchivo(modelo.img);
        const pathImg = path.join(__dirname, '../uploads/', coleccion, tipo, modelo.img);

        if (fs.existsSync(pathImg)) {
            fs.unlinkSync(pathImg);
        };
    };

    try {
        const nombreArchivo = await subirArchivo(req.files, coleccion);
        modelo.img = nombreArchivo;
        await modelo.save();
        res.json(modelo);
    } catch (msg) {
        res.status(400).json({ msg });
    }
};

const mostrarImagen = async (req = request, res = response) => {

    const { id, coleccion } = req.params;
    const notFound = path.join(__dirname, '../assets/no-image.jpg');
    const modelo = await obtenerModeloImagen(coleccion, id);

    if (!modelo) {
        return res.status(400).sendFile(notFound);
    };

    if (modelo.img) {
        const { tipo } = catalogarArchivo(modelo.img);
        const pathImg = path.join(__dirname, '../uploads/', coleccion, tipo, modelo.img);

        if (fs.existsSync(pathImg)) {
            return res.status(200).sendFile(pathImg);//Devuelve la imagen del servidor
        }
    };
    res.status(404).sendFile(notFound);
};

module.exports = {
    actualizarImagen,
    cargarArchivos,
    mostrarImagen,
};
