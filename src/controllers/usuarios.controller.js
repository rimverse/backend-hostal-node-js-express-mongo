const { request, response } = require('express');
const bcryptjs = require('bcryptjs');

const { Usuario } = require('../models');

const listarUsuarios = async (req = request, res = response) => {

    const query = { estado: true };

    const [total, usuarios] = await Promise.all([
        Usuario.countDocuments(query),
        Usuario.find(query)
    ]);

    res.json({
        total,
        usuarios
    });
};

const detalleUsuario = async (req = request, res = response) => {

    const { id } = req.params;
    const detalleUsuario = await Usuario.findById(id);
    res.json(detalleUsuario);
};

const crearUsuario = async (req = request, res = response) => {

    const { nombres, apellidos, correo, password, rol, dni, telefono } = req.body;
    const usuario = new Usuario({ nombres, apellidos, correo, password, rol, dni, telefono });

    const numeroVueltas = bcryptjs.genSaltSync();
    usuario.password = bcryptjs.hashSync(password, numeroVueltas);

    await usuario.save();
    res.json(usuario);
};

const editarUsuario = async (req = request, res = response) => {

    const { id } = req.params;
    const { estado, password, ...usuario } = req.body;

    if (password) {
        const numeroVueltas = bcryptjs.genSaltSync();
        usuario.password = bcryptjs.hashSync(password, numeroVueltas);
    };
    const usuarioEditado = await Usuario.findByIdAndUpdate(id, usuario, { new: true });
    res.json(usuarioEditado);
};

const eliminarUsuario = async (req = request, res = response) => {

    const { id } = req.params;
    const usuarioEliminado = await Usuario.findByIdAndUpdate(id, { estado: false }, { new: true });
    res.json(usuarioEliminado);
};

module.exports = {
    crearUsuario,
    detalleUsuario,
    editarUsuario,
    eliminarUsuario,
    listarUsuarios
}