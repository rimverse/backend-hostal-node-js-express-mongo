const { request, response } = require('express');
const bcryptjs = require('bcryptjs');

const { Usuario } = require('../models');
const { generarJWT } = require('../helpers');

const loginUser = async (req = request, res = response) => {

    const { correo, password } = req.body;

    try {

        //Verificar si el email existe
        const usuario = await Usuario.findOne({ correo });
        if (!usuario) {
            return res.status(400).json({
                msg: "correo"
            });
        };

        //Si el usuario esta activo
        if (!usuario.estado) {
            return res.status(400).json({
                msg: 'inactivo'
            });
        };

        //Verificar la contraseña
        const validPassword = bcryptjs.compareSync(password, usuario.password);
        if (!validPassword) {
            return res.status(400).json({
                msg: 'password'
            });
        };

        //Gnerar el jwt
        const token = await generarJWT(usuario._id);

        res.json({
            usuario,
            token
        });

    } catch (error) {
        console.log(error);
        return res.status(500).json({
            msg: 'Hable con el administrador'
        });
    };
};

module.exports = { loginUser };