const { request, response } = require("express");
const { Habitacion } = require("../models");

const buscarHabitaciones = async (req = request, res = response) => {

    const { termino } = req.params;

    const { estado = 'A' } = req.query;

    const regex = new RegExp(termino, 'i');

    const query = { $and: [{ estado }, { tipo: regex }] };

    const [total, habitaciones] = await Promise.all([
        Habitacion.countDocuments(query),
        Habitacion.find(query)
    ]);

    res.json({
        total,
        habitaciones
    });
};

module.exports = {
    buscarHabitaciones
}