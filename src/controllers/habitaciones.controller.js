const { request, response } = require('express');
const { Habitacion } = require('../models');

const listarHabitaciones = async (req = request, res = response) => {

    const { estado = 'A' } = req.query;
    const query = { estado };

    const [total, habitaciones] = await Promise.all([
        Habitacion.countDocuments(query),
        Habitacion.find(query)

    ]);

    res.json({
        total, habitaciones
    })
};

const detalleHabitacion = async (req = request, res = response) => {

    const { id } = req.params;
    const habitacionBuscada = await Habitacion.findById(id);
    res.json(habitacionBuscada);
};

const crearHabitacion = async (req = request, res = response) => {

    const { tipo, piso, numero, precio, aforo } = req.body;
    const habitacion = new Habitacion({ tipo, piso, numero, precio, aforo });
    await habitacion.save();
    res.json(habitacion);
};

module.exports = {
    crearHabitacion,
    detalleHabitacion,
    listarHabitaciones
};