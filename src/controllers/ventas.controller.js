const { request, response } = require("express");
const { Venta } = require("../models");


const listarVentas = async (req = request, res = response) => {

    const { consulta } = req.query;

    res.json(consulta);

};

const guardarVenta = async (req = request, res = response) => {

    const venta = new Venta({});

    await venta.save();

    res.json(venta);
};

const editarVenta = async () => {

};

module.exports = {
    guardarVenta,
    listarVentas
}