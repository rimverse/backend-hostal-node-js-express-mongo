const { request, response } = require('express');
const { Categoria } = require('../models');

const listarCategorias = async (req, res = response) => {

    const categorias = await Categoria.find();
    res.json({ categorias });
};

const crearCategoria = async (req = request, res = response) => {

    const { nombre } = req.body;
    const categoria = new Categoria({ nombre: nombre.substring(0, 1).toUpperCase() + nombre.substring(1) });
    await categoria.save();
    res.json(categoria);

};

module.exports = {
    crearCategoria,
    listarCategorias
}