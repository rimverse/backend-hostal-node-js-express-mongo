const { model, Schema } = require('mongoose');

const ProductoSchema = new Schema({
    nombre: { type: String, required: [true, 'El nombre del producto es requerido'] },
    categoria: { type: Schema.Types.ObjectId, ref: 'Categoria', required: true },
    precio: { type: String, required: [true, 'El precio del producto es requerido'] },
    cantidad: { type: Number },
    disponible: { type: Boolean, default: true },
    img: { type: String }
}, {
    versionKey: false
});

module.exports = model('Producto', ProductoSchema, 'Productos')