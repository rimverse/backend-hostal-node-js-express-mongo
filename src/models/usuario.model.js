const { Schema, model } = require('mongoose');

const UsuarioSchema = new Schema({
    nombres: { type: String, required: [true, 'El nombre es obligatorio'] },
    apellidos: { type: String, required: [true, 'El apellido es obligatorio'] },
    correo: { type: String, required: [true, 'El correo es obligatorio'], unique: true },
    password: { type: String, required: [true, 'La contraseña es obligatoria'] },
    rol: { type: String, default: 'user_role', required: true },
    dni: { type: String, required: [true, 'El dni es obligatorio'], unique: true },
    telefono: { type: String, required: [true, 'El telefono es obligatorio'], unique: true },
    img: { type: String },
    estado: { type: Boolean, default: true }
}, {
    versionKey: false
});

UsuarioSchema.methods.toJSON = function () {
    const { _id, password, ...usuario } = this.toObject();
    usuario.uid = _id;
    return usuario;
};

module.exports = model('Usuario', UsuarioSchema, 'Usuarios');


