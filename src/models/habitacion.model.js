const { Schema, model } = require('mongoose');

const HabitacionSchema = new Schema({
    tipo: { type: String, required: [true, 'El tipo de habitación es obligatorio'] },
    piso: { type: String, required: [true, 'El piso de la habitación es obligatorio'] },
    numero: { type: String, required: [true, 'El número de la habitación es obligatorio'] },
    precio: {
        porHoras: { type: Number, required: [true, 'El precio por horas es obligatorio'] },
        porDia: { type: Number, required: [true, 'El precio por dia es obligatorio'] },
    },
    img: { type: String },
    aforo: { type: String, default: '2' },
    estado: { type: String, default: 'A' },//A: activo, O: ocupado, C: cerrado, M: mantenimiento
}, {
    versionKey: false
});

HabitacionSchema.methods.toJSON = function () {
    const { _id, ...habitacion } = this.toObject();
    habitacion.uid = _id;
    return habitacion;
};

module.exports = model('Habitacion', HabitacionSchema, 'Habitaciones');