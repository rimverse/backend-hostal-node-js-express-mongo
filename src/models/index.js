const Categoria = require('./categoria.model');
const Habitacion = require('./habitacion.model');
const Rol = require('./rol.model');
const Producto = require('./producto.model');
const Usuario = require('./usuario.model');
const Venta = require('./venta.model');

module.exports = {
    Categoria,
    Habitacion,
    Rol,
    Producto,
    Usuario,
    Venta
};