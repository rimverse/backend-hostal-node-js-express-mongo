const express = require('express');
const cors = require('cors');

const { dbConnection } = require('../database/connection');
const fileUpload = require('express-fileupload');

class Server {

    constructor() {

        this.rutas = {
            authPath: '/api/auth',
            busquedasPath: '/api/buscar',
            categoriasPath: '/api/categorias',
            habitacionesPath: '/api/habitaciones',
            productosPath: '/api/productos',
            uploadsPath: '/api/uploads',
            usuariosPath: '/api/usuarios',
            ventasPath: '/api/ventas'
        };

        this.app = express();
        this.port = process.env.PORT;

        this.connectionDB();
        this.middlewares();
        this.routes();
    };

    async connectionDB() {
        await dbConnection();
    };

    middlewares() {

        this.app.use(express.json());
        this.app.use(cors());

        this.app.use(fileUpload({
            useTempFiles: true,
            tempFileDir: '/tmp/',
            createParentPath: true
        }));

    };

    routes() {
        this.app.use(this.rutas.authPath, require('../routes/auth.route'));
        this.app.use(this.rutas.busquedasPath, require('../routes/busquedas.route'))
        this.app.use(this.rutas.habitacionesPath, require('../routes/habitaciones.route'));
        this.app.use(this.rutas.categoriasPath, require('../routes/categorias.route'));
        this.app.use(this.rutas.productosPath, require('../routes/productos.route'));
        this.app.use(this.rutas.uploadsPath, require('../routes/uploads.route'));
        this.app.use(this.rutas.usuariosPath, require('../routes/usuarios.route'));
        this.app.use(this.rutas.ventasPath, require('../routes/usuarios.route'));
    };

    listen() {
        this.app.listen(this.port, () => {
            console.log(`Servidor corriendo en el puerto: ${this.port}`);
        });
    };
};

module.exports = Server;