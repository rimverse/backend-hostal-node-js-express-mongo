const { model, Schema } = require('mongoose');

const VentaSchema = new Schema({
    cliente: { type: String, required: [true, 'El nombre del cliente es obligatorio'] },
    dni: { type: String, required: [true, 'El dni del cliente es obligatorio'] },
    habitacion: { type: Schema.Types.ObjectId, ref: 'Habitacion', required: true },
    recepcionista: { type: Schema.Types.ObjectId, ref: 'Usuario', required: true },
    estado: { type: String, default: 'E' }, //E: en curso, T: Terminado
    productos: [

    ],
    total: { type: Number, required: [true, 'El total es obligatorio'] },

}, {
    versionKey: false,
});


module.exports = model('Venta', VentaSchema, 'Ventas')