const { model, Schema } = require('mongoose');

const CategoriaSchema = new Schema({
    nombre: { type: String, unique: true, required: [true, 'El nombre es obligatorio'] },
    estado: { type: Boolean, default: true, required: true },
}, {
    versionKey: false
});

module.exports = model('Categoria', CategoriaSchema, 'Categorias');