
const { connect } = require('mongoose');

const dbConnection = async () => {
    try {
        await connect(process.env.MONGODB_CNN, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        console.log('Base de datos online!');

    } catch (error) {
        console.log(error);
        throw new Error('Error a la hora de inciar la base de datos');
    }
};

module.exports = { dbConnection };