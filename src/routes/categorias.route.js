const { Router } = require('express');

const { listarCategorias, crearCategoria } = require('../controllers/categorias.controller');

const { check } = require('express-validator');
const { validarCampos } = require('../middlewares');

const router = Router();

router.get('/listar', listarCategorias);

router.post('/crear', [
    check('nombre', 'El nombre es obligatorio').not().isEmpty(),
    validarCampos
], crearCategoria);


module.exports = router;