const { Router } = require('express');
const { check } = require('express-validator');

//Mis controladores
const { cargarArchivos, actualizarImagen, mostrarImagen, editPhoto } = require('../controllers/uploads.controller');

//Helpers y middlewares
const { validarArchivoSubir, validarCampos, validarJWT } = require('../middlewares');
const { coleccionesPermitidas } = require('../helpers');

const router = Router();

router.get('/mostrar/:coleccion/:id', [
    check('id', 'No es un ID válido de Mongo').isMongoId(),
    check('coleccion').custom(c => coleccionesPermitidas(c, ['habitaciones', 'usuarios', 'productos'])),
    validarCampos
], mostrarImagen);

router.post('/:coleccion', [
    validarArchivoSubir,
    check('coleccion').custom(c => coleccionesPermitidas(c, ['habitaciones', 'usuarios', 'productos'])),
    validarCampos
], cargarArchivos);

router.put('/editar/:coleccion/:id', [
    validarJWT,
    validarArchivoSubir,
    check('id', 'No es un ID válido de Mongo').isMongoId(),
    check('coleccion').custom(c => coleccionesPermitidas(c, ['habitaciones', 'usuarios', 'productos'])),
    validarCampos
], actualizarImagen);


module.exports = router;