const { Router } = require('express');
const { check } = require('express-validator');

const { listarUsuarios, crearUsuario, detalleUsuario, editarUsuario, eliminarUsuario } = require('../controllers/usuarios.controller');
const { validarCampos, esAdminRole, validarJWT } = require('../middlewares');
const { existeEmail, esRolValido, existeDni, existeUsuarioPorId } = require('../helpers');

const router = Router();

router.get('/listar', listarUsuarios);

router.get('/detalle/:id', [
    check('id', 'No es un ID válido de Mongo').isMongoId(),
    check('id').custom(existeUsuarioPorId),
    validarCampos
], detalleUsuario);

router.post('/crear', [
    esAdminRole,
    check('nombres', 'El nombre es obligatorio').not().isEmpty(),
    check('apellidos', 'El apellido es obligatorio').not().isEmpty(),
    check('correo', 'El correo no es válido').isEmail(),
    // check('correo').custom(existeEmail),
    check('password', 'El password debe ser de más de 8 letras').isLength({ min: 8 }),
    check('rol').custom(esRolValido),
    check('dni', 'El DNI debe tener 8 números').isLength({ min: 8, max: 8 }),
    // check('dni').custom(existeDni),
    check('telefono', 'El teléfono debe tener 9 números ').isLength({ min: 9, max: 9 }),
    validarCampos
], crearUsuario);

router.put('/editar/:id', [
    validarJWT,
    check('id', 'No es un ID válido de Mongo').isMongoId(),
    check('id').custom(existeUsuarioPorId),
    check('rol').custom(esRolValido),
    validarCampos
], editarUsuario);

router.delete('/eliminar/:id', [
    validarJWT,
    esAdminRole,
    check('id', 'No es un ID válido de Mongo').isMongoId(),
    check('id').custom(existeUsuarioPorId),
    validarCampos
], eliminarUsuario);

module.exports = router;