const { Router } = require('express');
const { check } = require('express-validator');

const { listarHabitaciones, crearHabitacion, detalleHabitacion } = require('../controllers/habitaciones.controller');
const { validarCampos, esAdminRole } = require('../middlewares');
const { existeHabitacionPorId } = require('../helpers');

const router = Router();

router.get('/listar', listarHabitaciones);

router.get('/detalle/:id', [
    check('id', 'No es una id válido de Mongo').isMongoId(),
    check('id').custom(existeHabitacionPorId),
    validarCampos
], detalleHabitacion);

router.post('/crear', [
    esAdminRole,
    check('tipo', 'El tipo de habitación es obligatorio').not().isEmpty(),
    check('piso', 'El piso de la habitación es obligatorio').not().isEmpty(),
    check('numero', 'El número de la habitación es obligatorio').not().isEmpty(),
    check('precio', 'El precio es obligatorio').not().isEmpty(),
    check('precio.porHoras', 'El precio por horas es obligatorio').not().isEmpty(),
    check('precio.porHoras', 'El precio por horas debe ser un número').isNumeric(),
    check('precio.porDia', 'El precio por día es obligatorio').not().isEmpty(),
    check('precio.porDia', 'El precio por día debe ser un número').isNumeric(),
    validarCampos
], crearHabitacion);

module.exports = router;