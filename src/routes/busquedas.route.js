const { Router } = require("express");
const { buscarHabitaciones } = require("../controllers/busquedas.controller");

const router = Router();

router.get('/:termino', buscarHabitaciones)

module.exports = router;