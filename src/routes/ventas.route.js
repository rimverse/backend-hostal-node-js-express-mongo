const { Router } = require("express");
const { listarVentas } = require("../controllers/ventas.controller");


const router = Router();

router.get('/listar', listarVentas);

module.exports = router;    