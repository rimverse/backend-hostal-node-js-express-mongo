const { Router } = require("express");
const { listarProductos, crearProducto } = require("../controllers/productos.controller");

const router = Router();

router.get('/listar', listarProductos);

router.post('/crear', crearProducto);
// router.delete();

module.exports = router;